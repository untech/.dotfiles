" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2011 Apr 15
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" add Pathogen
execute pathogen#infect()

" nim-related
fun! JumpToDef()
  if exists("*GotoDefinition_" . &filetype)
    call GotoDefinition_{&filetype}()
  else
    exe "norm! \<C-]>"
  endif
endf

" Comfortable motion
let g:comfortable_motion_interval = 5000.0 / 60
let g:comfortable_motion_friction = 800.0
let g:comfortable_motion_air_drag = 20.0

" Jump to tag
nn <M-g> :call JumpToDef()<cr>
ino <M-g> <esc>:call JumpToDef()<cr>i

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

"if has("vms")
"  set nobackup		" do not keep a backup file, use versions instead
"else
"  set backup		" keep a backup file
"endif
set nobackup
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set number		" номера строк
set incsearch		" do incremental searching
set ts=4
set sw=4 "размер табуляций
"set nowrap " отключение переноса строк
set expandtab " заменять табуляции пробелами
" сохранение по F2:
imap <F2> <ESC>:w<CR>
map <F2> <ESC>w<CR>
imap <F5> <ESC>:.!date +\[\%A,\ \%d.\%m.\%y\ \%H:\%M\]<CR>%
map <F5> <ESC>:.!date +\[\%A,\ \%d.\%m.\%y\ \%H:\%M\]<CR>%

set list
set listchars=tab:\:\ ,trail:$,extends:~
set t_Co=256 "256 цветов в терминале
set title "Имя файла в заголовке
" set undofile "Бесконечный Undo
"Нормальная работа j и k
nnoremap j gj
nnoremap k gk
" Поддержка русской раскладки
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

" LaTeX support
filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

" Tab navigation
nnoremap th  :tabfirst<CR>
nnoremap tk  :tabnext<CR>
nnoremap tj  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnext<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>

" Free Pascal support
let pascal_delphi=1
let pascal_fpc=1

" 80th column warning
" set colorcolumn=80

"For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" speed up scroll
" set ttyfast

" Display 3 lines above/below the cursor when scrolling
set scrolloff=3


" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  "colorscheme 256-jungle


  if has('termguicolors')
    " set termguicolors  " if uncommented, brokes color in tmux for Ubuntu
  endif


  let g:sonokai_style = 'shusia'
  let g:sonokai_enable_italic = 0
  let g:sonokai_disable_italic_comment = 1
  let g:sonokai_transparent_background = 0
  colorscheme sonokai

  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on
  let g:pydiction_location = '/usr/share/pydiction/complete-dict'

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif
